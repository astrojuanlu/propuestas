---
layout: 2021/post
section: proposals
category: devrooms
community: COMMUNITY
title: DEVROOM-NAME
---

## Devroom description

[DEVROOM DESCRIPTION: Explanation of the subject of the devroom and the motivation to organize it. Keep in mind that the idea of the devroom is to carry out a semi-autonomous activity within esLibre, which will normally have its own proposals submission, its own organizers, its own schedule (coordinated with that of the congress), etc.]

## Community that proposes it

**[Name of the community that proposes it]**

[DEVROOM DESCRIPTION: Explanation of the subject of the devroom and the motivation to organize it. Keep in mind that the idea of the devroom is to carry out a semi-autonomous activity within esLibre, which will normally have its own proposals submission, its own organizers, its own schedule (coordinated with that of the congress), etc.]

-   Community website: [URL]
-   Mastodon (or other free social networks): [URL]
-   Twitter: [URL]
-   Gitlab: [URL]
-   Portfolio or GitHub (or other collaborative code sites): [URL]

### Contact/s

-   Contact name: [NAME]
-   Contact email: [EMAIL]

## Target audiences

[TARGET AUDIENCES: To whom?]

## Format

[FORMAT: Format of the devroom and how the proposals submission will be made. The format is left completely open to the decision of the organization of the devroom, and may include: workshops, short/long talks, round tables, hackathons...]

## Time preference

-   Duration: [Indicate the duration of the proposal. For example, from two hours to a whole day.]
-   Day: [Indicate if you prefer it to be the first day or the second.]

## Comments

[COMMENTS: Any other comment relevant to the organization.]

## Privacy preferences

(If you want your contact information to be anonymous, you can send us the proposals using the forms on the web: <https://eslib.re/2021/proposals/devrooms/>)

-   [x]  We give permission for our contact email to be published with the room information.

## Accepted conditions

-   [x]  We agree to follow the [conduct code](https://eslib.re/conduct/) and to ask those attending to comply with it.
-   [x]  We accept to coordinate us with the esLibre organization for the realization of the devroom.
-   [x]  We accept that the room can be withdrawn if there is no program ready for the deadline.
-   [x]  We confirm that at least one person will be online on the day scheduled to coordinate the room.
