---
layout: 2021/post
section: proposals
category: devrooms
community: Málaga Scala Developers / LibreLabGRX
title: Aλhambra Day&#58 Programación funcional para todo el mundo
---

## Descripción de la sala

La Programación Funcional no es esa asignatura-cardo que hay que aprobar una vez en la vida. Tampoco es una ladera llena de chumberas que hay que escalar para comprender lo que son las mónadas o la Teoría de Categorías. Se trata de una herramienta más que puede ayudarnos un montón en problemas a los que nos enfrentamos en nuestros trabajos y estudios. Te lo demostraremos.

Queremos dar lugar a charlas enfocadas a principiantes que no tienen experiencia con la Programación Funcional, pero que conocen cualquier otro lenguaje como Python, Ruby, Java o C. ¿Cómo iniciarse en el mundo de la Programación Funcional?

También pensamos en todas aquellas personas que están todavía en un eterno nivel intermedio de Scala, pero que no tienen tiempo para avanzar. ¿Qué camino seguir?

Sea cual sea tu experiencia, tenemos algo para tí.

El responsables de esta sala será Dawid Furman: Scala developer, entusiasta de la programación funcional y organizador en Málaga Scala Developers.

## Comunidad que la propone

#### Málaga Scala Developers / LibreLabGRX

Málaga Scala Developers es un grupo creado con el propósito de compartir experiencias sobre Scala y las distintas tecnologías que forman parte de su ecosistema: frameworks de desarrollo Web como Play o Lift, la infraestructura de middleware basada en actores Akka, frameworks de prueba BDD como Scalatest o Specs, etc.

LibreLabGRX es un grupo de personas interesadas en el software/hardware libre y la cultura abierta de Granada, lo que les ha llevado a apoyar y participar en la organización de actividades como el evento precedente que da nombre a esta sala (Aλhambra Day) o la primera edición de esLibre.

Ya realizamos actividades similares en la primera edición edición de [Aλhambra Day](https://www.eventbrite.es/e/entradas-ahambra-day-57993381766#), y de no ser por la situación actual, hubieramos repetido en [Aλhambra Day II](https://librelabgrx.cc/lambda-alhambra-ii/).

-   Web de la comunidad: <https://www.meetup.com/es-ES/Malaga-Scala/> / <https://librelabgrx.cc/>
-   Mastodon (u otras redes sociales libres): <https://floss.social/@librelabgrx>
-   Twitter: [@MalagaScala](https://twitter.com/MalagaScala) / [@LibreLabGRX](https://twitter.com/LibreLabGRX)
-   Gitlab: [@LibreLabGRX](https://gitlab.com/librelabgrx)
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/MalagaScala>

### Contacto(s)

-   Nombre de contacto: Dawid Furman
-   Email de contacto: <lambda.in.alhambra@librelabgrx.cc>

## Público objetivo

Cualquier persona con interés en adentrarse en la programación funcional o ampliar sus conocimientos sobre la misma.

## Formato

Varias charlas de diversos niveles, y de ser posible algún taller.

## Preferencia horaria

-   Duración: En principio medio día.
-   Día: Indistinto.

## Comentarios

La intención es hacer una mezcla entre charlas invitadas y envío de propuestas, tanto en español como en inglés.

## Preferencias de privacidad

-   [x]  Damos permiso para que nuestro email de contacto sea publicado con la información de la sala.

## Condiciones aceptadas

-   [x]  Aceptamos seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a las personas asistentes su cumplimiento.
-   [x]  Aceptamos coordinarnos con la organización de esLibre para la organización de la sala.
-   [x]  Aceptamos que la sala puede ser retirada si no hay un programa terminado para la fecha decidida por la organización.
-   [x]  Confirmamos que al menos una persona de entre las que proponen la sala estará conectada el día programado para realizarla.
