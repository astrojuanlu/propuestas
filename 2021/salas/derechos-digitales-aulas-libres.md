---
layout: 2021/post
section: proposals
category: devrooms
community: Interferencias
title: Derechos Digitales, Privacidad en Internet y Seguridad Informática + Soberanía Digital en las Aulas (Fuera Google)
---

## Descripción de la sala

Se ha demostrado en los últimos meses que la colaboración, la autogestión y las comunidades constructivas son la clave para hacer funcionar el mundo frente a toda corriente. En esta situación, el software y el hardware libre tienen un hueco especial, conformado por una comunidad multidisciplinar dispuesta a colaborar por avance tecnológico responsable. La comunidad de software/hardware libre ahora más que nunca debe estar dispuesta a crear herramientas prácticas que ayuden a la sociedad a avanzar de manera transparente y segura.

Desde Interferencias apoyamos firmemente el software libre, creemos necesaria una tecnología responsable para incidir en varios elementos clave de la sociedad, además de la transparencia, la privacidad y los derechos digitales como base de cualquier proyecto. Uno de nuestros focos principales está en la educación, cuyas competencias tecnológicas se han acelerado en los últimos meses hacia recursos que poco cuidan la transparencia de su metodología. Romper la cadena de las herramientas privativas es una de las tareas más complicadas, pues se aprovechan de las dinámicas rápidas y fugaces en las que vivimos para obviar los muchos problemas que esconde el uso indebido de datos. Pero tenemos esperanza.

Desde 2016, Interferencias realiza unas Jornadas de Anonimato, Seguridad y Privacidad (JASYP) con la intención de acoger ideas diversas desde distintas áreas sobre estas temáticas, pero desde el pasado 2020 y mientras dure la situación sanitaria actual nos acogemos a esLibre para ofrecer este espacio de intercambio de ideas. Creemos que aunque la experiencia no es igual a la de las jornadas originales, el evento nos brinda un entorno seguro, abierto e inclusivo acorde con nuestros ideales.

Nos gustaría poder acoger proyectos de todo tipo que apoyen la tecnología segura, que apoye la privacidad de los usuarios, sea transparente y abierta. Sería estupendo contar con voces diversas, que nos cuenten cómo apoyan las mismas cosas que nosotras, y que nos brinden una oportunidad de aprender desde perspectivas distintas. Aceptamos charlas, talleres, propuestas y toda la clase de ideas que nos ayuden a crear un diálogo constructivo sobre la importancia y la necesidad de la defensa de los derechos digitales.

## Comunidad que la propone

#### Interferencias

Somos una asociación ciberactivista con interés en la defensa de la privacidad y los derechos digitales, además de la seguridad informática a nivel divulgativo, para lo que organizamos y compartimos información y opinión a través de nuestras redes sociales y grupo de Telegram. Todo esto sería imposible sin el software libre y las comunidades, y es por eso siempre nos gusta colaborar en iniciativas que nos parecen interesantes, como es la organización de esLibre: espacios que sirvan para reivindicar la importancia del conocimiento y la difusión del las tecnologías libres y cultura abierta.

Ya organizamos una sala de la misma temática en la edición anterior de esLibre: <https://interferencias.tech/eslibre2020/>

-   Web de la comunidad: <https://interferencias.tech/>
-   Mastodon (u otras redes sociales libres): <https://mastodon.technology/@interferencias>
-   Twitter: <https://twitter.com/Inter_ferencias>
-   GitLab: <https://gitlab.com/interferencias>
-   Portfolio o GitHub (u otros sitios de código colaborativo):

### Contacto(s)

-   Nombre de contacto: Paula de la Hoz / Germán Martínez
-   Email de contacto: <info@interferencias.tech>

## Público objetivo

Cualquier persona interesada en la defensa de los derechos digitales desde cualquier tipo de disciplina del conocimiento, ya que es algo que afecta a cualquier persona que tenga trato con la tecnología en su día a día.

## Formato

La sala se dividirá en dos bloques: un bloque que sería similar a la propuesta presentada en la edición del año pasado de eslibre, y el otro enfocado en el problema de la implantación masiva de software no respetuoso con la privacidad que se está haciendo en las aulas de colegios e institutos a un ritmo acelerado con la excusa de la situación sanitaria actual.

Para el primer bloque se seguirá el formato charlas y propuestas abiertas a cualquier persona interesada en hablar sobre la importancia de los derechos digitales, la privacidad en Internet, la seguridad informática y todos aquellos temas de este ámbito que puedan tener relación. Las charlas pueden ser de cualquier área. Es cierto que la temática se presta a talleres y charlas técnicas, pero nos encantaría acoger también charlas relacionadas con la temática desde un punto de vista distinto, como puede ser el de personas dedicadas a derecho, filosofía, política, arte o cualquier otra rama.

Para el segundo bloque nos pondremos en contacto con diferentes colectivos que estén intentando promover alternativas a los acuerdos que se están llegando desde las diferencias Administraciones, entregando de forma totalmente irresponsable la gestión de la educación a empresas como Google o Microsoft, empresas con una total falta de ética en lo que corresponde al respeto de la privacidad.

## Preferencia horaria

-   Duración: La duración total sería de un día, pero podría ser un día completo o la mitad de dos días.
-   Día: En principio es indistinto.

## Comentarios



## Preferencias de privacidad

-   [x]  Damos permiso para que nuestro email de contacto sea publicado con la información de la sala.

## Condiciones aceptadas

-   [x]  Aceptamos seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a las personas asistentes su cumplimiento.
-   [x]  Aceptamos coordinarnos con la organización de esLibre para la organización de la sala.
-   [x]  Aceptamos que la sala puede ser retirada si no hay un programa terminado para la fecha decidida por la organización.
-   [x]  Confirmamos que al menos una persona de entre las que proponen la sala estará conectada el día programado para realizarla.
