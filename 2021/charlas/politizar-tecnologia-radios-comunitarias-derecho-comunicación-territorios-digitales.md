---
layout: 2021/post
section: proposals
category: talks
author: Inés Binder / Santiago García Gago
title: Politizar la tecnología. Radios comunitarias y derecho a la comunicación en los territorios digitales
---

Hoy en día la defensa del derecho a la comunicación exige, necesariamente, la apuesta por la soberanía tecnológica. Los medios libres, alternativos y comunitarios tienen experiencia en subvertir una tecnología unidereccional como la radio para garantizar la participación de las audiencias. En esta charla nos preguntaremos cuál es el nuevo horizonte tecnológico en la defensa del derecho a la comunicación desde los medios libres y esbozaremos algunas respuestas.

## Formato de la propuesta

Indicar uno de estos:
-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

Las radios comunitarias siempre han acompañado la defensa de estos territorios y de sus bienes comunes. Desde sus micrófonos se desenmascararon las falsas promesas de las mineras y se denunció cómo su explotación contaminaba los ríos. Apoyaron la organización vecinal que luchaba por una vida digna en los barrios periféricos de las grandes ciudades. Amplificaron los reclamos de los campesinos frente al ecocidio de los cultivos extensivos de palma africana. Y reclamaron un espectro radioeléctrico más diverso que reflejara la realidad y la cultura de sus comunidades.

Hoy, estos territorios continúan siendo saqueados. Pero el asedio y la amenaza se extienden a los nuevos territorios digitales que habitamos. Entre las fronteras de Internet también existen fuerzas que colonizan y mercantilizan todos los aspectos de nuestra vida. Y de la misma manera, la ciudadanía resiste y se organiza alrededor de iniciativas que construyen autonomía y soberanía tecnológica.

En esta charla plantearemos cómo podemos entender las iniciativas de soberanía tecnológica desde el marco de la defensa del derecho a la comunicación, con el que vienen trabajando los medios libres y comunitarios desde hace décadas.

-   Web del proyecto: <https://radioslibres.net/politizar-la-tecnologia/>

## Público objetivo

La charla está dirigida al público general interesado por la soberanía tecnológica y su vinculación con el movimiento de medios comunitarios, alternativos y libres.

## Ponente(s)

Inés Binder es comunicadora social. Investiga temas de feminismo, tecnologías y políticas de comunicación. Es cofundadora del Centro de Producciones Radiofónicas (CPR) y parte del espacio hackfeminista la bekka.

Santiago García Gago es comunicador social. Investiga temas de tecnopolítica y medios comunitarios. Es parte de RadiosLibres.net, Radialistas.net y de la Red de Radios Comunitarias y Software Libre.

### Contacto(s)

-   Nombre: Inés Binder
-   Email:
-   Web personal: <https://radioslibres.net>
-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@radioslibres>
-   Twitter: <https://twitter.com/radioslibres>
-   GitLab:
-   Portfolio o GitHub (u otros sitios de código colaborativo):

## Comentarios



## Preferencias de privacidad

-   [ ]  Doy permiso para que mi email de contacto sea publicado con la información de la charla.
-   [x]  Doy permiso para que mis redes sociales sean publicadas con la información de la charla.

## Condiciones aceptadas

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a las personas asistentes su cumplimiento.
-   [x]  Confirmo que al menos una persona de entre las que proponen la charla estará conectada el día programado para exponerla.
