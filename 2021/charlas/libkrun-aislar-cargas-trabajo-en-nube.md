---
layout: 2021/post
section: proposals
category: talks
author: Sergio López
title: Usando libkrun para aislar cargas de trabajo en la nube
---

Esta charla es una introducción a libkrun, una biblioteca dinámica que permite que otros programas adquieran la capacidad de aislar cargas de trabajo mediante Virtualización, de forma sencilla y con el menor coste posible en recursos físicos del Host.

## Formato de la propuesta

Indicar uno de estos:

-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

Desplegar cargas de trabajo en entornos de la nube compartidos entre distintos clientes implica la necesidad de establecer estrategias más estrictas para aislar dichas cargas, con el objetivo de minimizar el riesgo de que haya filtraciones de información entre usuarios que comparten el mismo nodo.

libkrun es una biblioteca dinámica que permite que otros programas adquieran la capacidad de aislar cargas de trabajo mediante Virtualización. Combinada con un "runtime OCI" como "crun", permite que una carga se ejecute, simultáneamente, con aislamiento por "namespaces" y por Virtualización.

Permite, además, hacer uso de complementos de hardware como "AMD SEV" o "Intel TDX" para ejecutar dichas cargas con cifrado completo y verificable de forma remota.

Esta charla es una versión actualizada y en Español de "libkrun: KVM-based isolation for your workloads", presentada en la pasada DevConf.cz 2021.

-   Web del proyecto: <https://github.com/containers/libkrun>

## Público objetivo

Público con interés en contenedores, virtualización y seguridad en la nube.

## Ponente(s)

Sergio López, Senior Software Engineer en el equipo de Virtualización de Red Hat. Mantenedor de libkrun, krunvm, rust-vmm, virtiofsd-rs y QEMU-microvm, entre otros proyectos.

### Contacto(s)

-   Nombre: Sergio López
-   Email:
-   Web personal:
-   Mastodon (u otras redes sociales libres): [@slp@fosstodon.org](https://fosstodon.org/@slp)
-   Twitter: [@slpnix](https://twitter.com/slpnix)
-   Gitlab: [@slp](https://gitlab.com/slp)
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/slp/>

## Comentarios



## Preferencias de privacidad

-   [ ]  Doy permiso para que mi email de contacto sea publicado con la información de la charla.
-   [x]  Doy permiso para que mis redes sociales sean publicadas con la información de la charla.

## Condiciones aceptadas

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a las personas asistentes su cumplimiento.
-   [x]  Confirmo que al menos una persona de entre las que proponen la charla estará conectada el día programado para exponerla.
