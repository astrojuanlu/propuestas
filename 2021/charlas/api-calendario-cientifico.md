---
layout: 2021/post
section: proposals
category: talks
author: Jorge Aguilera
title: API Calendario Científico
---

El Calendario Científico es una iniciativa que pretende contribuir a acercar la cultura científica a la población más joven y crear referentes lo más cercanos posibles para ellos mediante la publicación diaría de una efeméride científica en diferentes lenguas.

En esta charla relámpago voy a contar cómo me involucré en el proyecto y los pasos que hemos seguido para conseguir diversificar los canales de comunicación

## Formato de la propuesta

Indicar uno de estos:

-   [x]  Charla corta (10 minutos)
-   [ ]  Charla (25 minutos)

## Descripción

En esta segunda edición del proyecto, a través de Twitter conocí sobre el proyecto y me animé a ayudarles para facilitarles la labor de publicar cada día el evento.

En la anterior edición se usó como medio de comunicación principal Twitter con envíos diarios manuales del evento (uno por cada lengua) lo que suponía un esfuerzo con los pocos recursos que se disponía. Así tras algunas conversaciones, en esta edición, hemos conseguido:

-   Crear una página web con enlaces
-   Twittear de forma automática todos los días y por cada lengua
-   Crear un canal de Telegram
-   Integrarlo en asistentes de voz como Alexa o GoogleHome
-   etc.

La página oficial del proyecto se encuentra en <http://www.igm.ule-csic.es/calendario-cientifico>.

## Público objetivo

El público objetivo es muy abierto pues no se va a entrar en detalle a ninguna parte técnica sino que se va a realizar una exposición de los pasos que se han ido tomando hasta alcanzar el resultado actual.

## Ponente(s)

-   Jorge Aguilera. Programador con muchos años de experiencia y con algunas charlas impartidas.

### Contacto(s)

-   Nombre: Jorge Aguilera
-   Email: <jorge.aguilera@puravida-software.com>
-   Web personal: <https://jorge.aguilera.sory>
-   Mastodon (u otras redes sociales libres): [@jagedn@mastodon.madrid](https://mastodon.madrid/@jagedn)
-   Twitter: [@jagedn](https://twitter.com/jagedn)
-   Gitlab: [@jorge.aguilera](https://gitlab.com/jorge.aguilera)
-   Portfolio o GitHub (u otros sitios de código colaborativo):

## Comentarios

La idea es explicar de forma muy sencilla cómo hemos usado

-   Github
-   Parseado ficheros csv, json
-   Creado actions para twitter y telegram
-   Vistazo a los asistentes de voz

## Preferencias de privacidad

-   [x]  Doy permiso para que mi email de contacto sea publicado con la información de la charla.
-   [x]  Doy permiso para que mis redes sociales sean publicadas con la información de la charla.

## Condiciones aceptadas

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a las personas asistentes su cumplimiento.
-   [x]  Confirmo que al menos una persona de entre las que proponen la charla estará conectada el día programado para exponerla.
