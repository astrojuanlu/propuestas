---
layout: 2020/post
section: proposals
category: talks
title: TerosHDL&#58 un IDE open source para HDL
---

La charla pretende ser una introducción a TerosHDL, un repaso de las funcionalidades presentes y en desarrollo y una muestra de cómo pueden usarlo desde principiantes hasta profesionales del sector.

## Formato de la propuesta

Indicar uno de estos:

-   [x]  Charla (25 minutos)
-   [ ]  Charla relámpago (10 minutos)

## Descripción

TerosHDL es un IDE open-source multiplataforma para FPGA que busca ofrecer a los desarrolladores de lenguajes de descripción de hardware muchas de las facilidades habituales de los lenguajes de programación software. TerosHDL abarca desde las funcionalidades clásicas de la mayoría de editores de código (como chequeo de sintaxis o una vista de la estructura del código) hasta la generación de documentación automatizada, test sobre múltiples simuladores, etc.

Es fácil identificar muchos entornos de desarrollo para lenguajes populares como C++, Java o Python, que además suelen ser libres o, al menos, cuentan con una versión “community”. Pero el campo de los IDEs FPGA es mucho más limitado. De hecho, TerosHDL surgió a partir de nuestras necesidades. Necesitábamos un entorno de trabajo que fuera personalizable, que se adaptara fácilmente al workflow del equipo de trabajo en vez de lo contrario y, además, que se pudiera integrar con muchas de las herramientas que la comunidad está desarrollando actualmente.

-   <https://youtu.be/V-k1rRPOa44>
-   <https://github.com/TerosTechnology/terosHDL>

## Público objetivo

Va dirigida tanto a desarrolladores de HDL (FPGA y ASIC) como a los que se están iniciando en el hardware libre.

## Ponente(s)

-   **Ismael Pérez**: Ingeniero Senior de FPGA en Kerajet. <https://www.linkedin.com/in/ispero/>
-   **Carlos Alberto Ruiz**: Ingeniero Senior de FPGA en Software Radio Systems Ltd. <https://www.linkedin.com/in/carlos-alberto-ruiz-fpga/>
-   **Alfredo Sáez**: Desarrollador Senior de Python en Woodswallow. <https://www.linkedin.com/in/asaezper/>

### Contacto(s)

-   **TerosTechnology**: terostechnology at gmail dot com

## Comentarios

Ninguno.

## Condiciones

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [x]  Al menos una persona entre los que la proponen estará presente el día programado para la charla.
