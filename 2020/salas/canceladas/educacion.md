---
layout: 2020/post
section: proposals
category: devrooms
title: Educación
---

La presencia del software libre en la educación no ha parado de crecer en los últimos años, una tendencia que es clara tanto en la universidad como en formación profesional y niveles educativos anteriores.

Aunque es evidente que las tecnologías libres pueden ayudar y contribuir a la hora de impartir diferentes contenidos de múltiples asignaturas, quizás sus mayores beneficios se obtienen cuando los valores del software libre se trabajan explícitamente en el aula.

El objetivo de esta sala es unir a expertos del mundo de las tecnologías libres y docentes de todos los niveles educativos para debatir acerca de los desafíos que nos encontramos en el mundo de la educación -y de los que tendremos que hacer frente en los próximos años- y cómo podrían ser abordados desde una perspectiva basada en tecnologías libres.

## Comunidad o grupo que lo propone

**[Programamos](https://programamos.es/)** es una organización sin ánimo de lucro que promueve la enseñanza de la programación y la robótica desde edades tempranas haciendo uso de tecnologías libres, con el objetivo de fomentar el desarrollo del pensamiento computacional y promover los valores de la cultura libre en todas las etapas educativas.

Programamos tiene amplia experiencia en la organización de conferencias y talleres de estas características, así como otro tipo de encuentros y jornadas. A continuación puedes encontrar algunos ejemplos en los que esta entidad ha participado:

-   [Workshop on computational thinking in education 2016](http://jemole.me/ct-workshop/) (Harvard, MA, USA)
-   [FLOSS education and computational thinking workshop @OSS 2016](http://jemole.me/2016-OSSEduWorkshop/) (Gothenburg, Sweden)
-   [Superprogramadores 2015, Code for the planet](https://www.youtube.com/watch?v=mTezss7TEZc&list=PL_nprC45Ob5u0p3tZSq4M1e413OP8RHMk&index=1) (Madrid, España)

### Contactos

-   **Jesús Moreno León**: jml at jemole dot me

## Público objetivo

Docentes, investigadores y técnicos de la industria interesados en las posibilidades del software libre en la educación.

## Tiempo

Medio día.

## Día

6 de junio de 2020.

## Formato

-   Se aceptarán charlas tanto en formato relámpago (10 min.) como en formato "tradicional" (25 min.), así como talleres "manos en la masa"
-   Cuando la sala sea aceptada (si finalmente es así, claro), se publicará el procedimiento para el envío de contribuciones.
-   Para que todo el mundo pueda organizar sus viajes, el programa de la sala estará listo y publicado al menos un mes antes del congreso, con los ponentes que puedan participar ya confirmados

## Comentarios

Ninguno.

## Condiciones

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [x]  Al menos una persona entre los que proponen la devroom estará presente el día agendado para la _devroom_.
-   [x]  Acepto coordinarme con la organización de esLibre.
-   [x]  Entiendo que si no hay un programa terminado para la fecha que establezca la organización, la _devroom_ podría retirarse.
