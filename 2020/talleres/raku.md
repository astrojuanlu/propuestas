---
layout: 2020/post
section: proposals
category: workshops
title: Introducción a Raku
---

Raku es un lenguaje multiparadigma, cuyo desarrollo se inició hace 20 años, recibiendo un nuevo nombre en el año 2019 (era Perl 6). En este taller haremos una introducción al lenguaje y lo que es capaz de hacer, con el objetivo de que el asistente, al final del mismo, sea capaz de crear y ejecutar pequeños programas usándolo.

## Objetivos a cubrir en el taller

1. Qué es Raku, qué relación tiene con Perl, un poco de historia.
2. Cómo empezar a trabajar con Raku.
3. Literales.
4. Raku funcional
5. A dónde ir desde aquí.

## Público objetivo

Cualquier persona con mínimos conocimientos de programación en cualquier lenguaje, que quiera aprender un lenguaje nuevo.

## Ponente(s)

**JJ Merelo**. Trabajando con Raku desde 2016, colaborador del proyecto Raku y "documentador-en-jefe".

### Contacto(s)

-   JJMerelo: JJ5 @ GitLab, JJ @ GitHub

## Prerrequisitos para los asistentes

Ninguno, pero si quieren llevar a cabo alguno de los ejemplos conviene que se traigan el ordenador. 

## Prerrequisitos para la organización

El normal en la organización.

## Tiempo

Entre 60 y 90 minutos

## Día

Indiferente

## Comentarios

N/A

## Condiciones

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/).
-   [x]  Al menos una persona entre los que proponen el taller estará presente el día programado para el mismo.
-   [x]  Acepto coordinarme con la organización de esLibre.
